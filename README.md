## SUM Global Spring Boot Rule for Bazel
The following dependencies are added to the project and should be defined in your workspace.
```
# Macro to add spring web
def _add_web_deps(deps):
  _safe_add(deps, "@maven//:org_springframework_spring_web")


# Default Dependencies

# Add in the standard Spring Boot dependencies so that app devs don't
# need to explicitly state them every time in the BUILD file.
def _add_default_deps(deps):
  _safe_add(deps, "@maven//:javax_servlet_javax_servlet_api")

  _safe_add(deps, "@maven//:org_springframework_spring_beans")
  _safe_add(deps, "@maven//:org_springframework_spring_context")
  _safe_add(deps, "@maven//:org_springframework_spring_core")
  _safe_add(deps, "@maven//:org_springframework_spring_aop")
  _safe_add(deps, "@maven//:org_springframework_spring_expression")

  _safe_add(deps, "@maven//:org_springframework_boot_spring_boot")
  _safe_add(deps, "@maven//:org_springframework_boot_spring_boot_autoconfigure")
  _safe_add(deps, "@maven//:org_springframework_boot_spring_boot_loader")
  _safe_add(deps, "@maven//:org_springframework_boot_spring_boot_starter")
  _safe_add(deps, "@maven//:org_springframework_boot_spring_boot_starter_logging")



# Bazel will fail if a dependency appears twice for the same target, so be safe when
# adding a dependencies to the deps list
def _safe_add(deps, dep):
  if not (dep in deps):
    deps.append(dep)
```


### (Forked Salesforce Spring Boot Rule for Bazel)

This WORKSPACE contains the Spring Boot rule for the [Bazel](https://bazel.build/) build system.
It enables Bazel to build Spring Boot applications and package them as an executable jar file.

The rule is contained in a directory that is designed to be copied into your WORKSPACE.
It has detailed documentation:
- [bazel-springboot-rule](tools/springboot): a Bazel extension to build Spring Boot applications
